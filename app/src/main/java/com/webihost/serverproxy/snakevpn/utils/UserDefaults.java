package com.webihost.serverproxy.snakevpn.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class UserDefaults {

    private static UserDefaults standard;
    private static Context context;
    private final SharedPreferences sharedPreferences;

    public static void setContext(Context context) {
        UserDefaults.context = context;
    }

    private UserDefaults() {
        sharedPreferences = context.getApplicationContext().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
    }

    public static synchronized UserDefaults standard() {
        if (standard == null) {
            standard = new UserDefaults();
        }
        return standard;
    }

    public void putBoolean(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key, boolean def) {
        return sharedPreferences.getBoolean(key, def);
    }


    public void putInteger(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public int getInteger(String key, int def) {
        return sharedPreferences.getInt(key, def);
    }


    public void putLong(String key, long value) {
        sharedPreferences.edit().putLong(key, value).apply();
    }

    public long getLong(String key, long def) {
        return sharedPreferences.getLong(key, def);
    }


    public void putString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public String getString(String key, String def) {
        return sharedPreferences.getString(key, def);
    }
}
