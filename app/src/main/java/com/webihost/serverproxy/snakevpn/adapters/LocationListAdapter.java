package com.webihost.serverproxy.snakevpn.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anchorfree.partner.api.data.Country;
import com.northghost.hydraclient.R;
import com.pixplicity.easyprefs.library.Prefs;
import com.webihost.serverproxy.snakevpn.LockServerModel;
import com.webihost.serverproxy.snakevpn.MainApp;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> {

    private List<Country> regions;
    private List<LockServerModel> regionsLock;
    private final RegionListAdapterInterface listAdapterInterface;
    private final Context context;

    private final ArrayList<String> premiumServers = new ArrayList<>();


    public LocationListAdapter(Context context, RegionListAdapterInterface listAdapterInterface) {
        this.listAdapterInterface = listAdapterInterface;
        this.context = context;

        premiumServers.add("United States");
        premiumServers.add("Australia");
        premiumServers.add("Switzerland");
        premiumServers.add("Hong Kong");
        premiumServers.add("Denmark");
        premiumServers.add("Canada");
        premiumServers.add("Germany");
        premiumServers.add("Japan");
        premiumServers.add("United Kingdom");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.region_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Country country = regions.get(position);
        Locale locale = new Locale("", regions.get(position).getCountry());

        if (country.getCountry() != null && !country.getCountry().equals("")) {
            holder.regionTitle.setText(locale.getDisplayCountry());
            String str = regions.get(position).getCountry();
            holder.regionImage.setImageResource(MainApp.getStaticContext().getResources().getIdentifier("drawable/" + str, null, MainApp.getStaticContext().getPackageName()));

            if (position < regionsLock.size()) {

                if (premiumServers.contains(locale.getDisplayCountry())) {
                    regionsLock.get(position).setLock(true);

                }

            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listAdapterInterface.onCountrySelected(regions.get(holder.getAdapterPosition()));
                    Prefs.putString("sname", regions.get(position).getCountry());
                    Prefs.putString("simage", regions.get(position).getCountry());
                }
            });
        } else {
            holder.regionTitle.setText("Unknown Server");
            holder.setClicks();
        }
    }

    @Override
    public int getItemCount() {
        return regions != null ? regions.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.region_title)
        TextView regionTitle;

        @BindView(R.id.region_image)
        ImageView regionImage;

        @BindView(R.id.parent)
        LinearLayout cardView;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void setClicks() {
            regionTitle.setClickable(false);
            regionImage.setClickable(false);
            cardView.setClickable(false);

            regionTitle.setFocusable(false);
            regionImage.setFocusable(false);
            cardView.setFocusable(false);
        }
    }

    public void setRegions(List<Country> list) {
        regions = new ArrayList<>();
        regions.add(new Country(""));
        regions.addAll(list);

        regionsLock = new ArrayList<>(list.size());

        for (int i = 0; i < list.size(); i++) {
            regionsLock.add(i, new LockServerModel());
        }

        notifyDataSetChanged();
    }

    public interface RegionListAdapterInterface {
        void onCountrySelected(Country item);
    }
}
