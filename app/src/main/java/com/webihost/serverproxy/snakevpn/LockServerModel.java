package com.webihost.serverproxy.snakevpn;

public class LockServerModel {

    private boolean lock;

    public LockServerModel() {
        lock = false;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }
}

