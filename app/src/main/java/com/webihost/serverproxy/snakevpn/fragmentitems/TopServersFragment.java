package com.webihost.serverproxy.snakevpn.fragmentitems;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anchorfree.partner.api.data.Country;
import com.northghost.hydraclient.R;
import com.webihost.serverproxy.snakevpn.MainApp;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TopServersFragment extends Fragment {

    @BindView(R.id.freeServersRecyclerview)
    RecyclerView VIPServersRecyclerview;

    private com.webihost.serverproxy.snakevpn.fragmentitems.PassServerData mCallback;

    private final VIPServersAdapter adapter = new VIPServersAdapter();
    private List<Country> list = new ArrayList<>();

    public void registerCallback(com.webihost.serverproxy.snakevpn.fragmentitems.PassServerData passServerData) {
        mCallback = passServerData;
    }

    public static Fragment createInstance() {
        TopServersFragment vipServersFragment = new TopServersFragment();
        return vipServersFragment;
    }

    public void setVIPServersList(List<Country> servers) {
        list = servers;
        adapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_top_servers, container, false);
        ButterKnife.bind(this, view);

        VIPServersRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        VIPServersRecyclerview.setAdapter(adapter);

        return view;
    }

    class VIPServersAdapter extends RecyclerView.Adapter<VIPServersViewHolder> {

        @NonNull
        @Override
        public VIPServersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.region_list_item, parent, false);
            return new VIPServersViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull VIPServersViewHolder holder, int position) {
            holder.populateView(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }


    class VIPServersViewHolder extends RecyclerView.ViewHolder {

        private final TextView mRegionTitle;
        private final CircleImageView mRegionImage;
        private RelativeLayout mLockLayout;
        private final LinearLayout mItemView;

        public VIPServersViewHolder(@NonNull View itemView) {
            super(itemView);

            mRegionTitle = itemView.findViewById(R.id.region_title);
            mRegionImage = itemView.findViewById(R.id.region_image);

            mItemView = itemView.findViewById(R.id.itemView);
        }

        public void populateView(Country country) {


            Locale locale = new Locale("", country.getCountry());
            mRegionTitle.setText(locale.getDisplayCountry());
            mRegionImage.setImageResource(MainApp.getStaticContext().getResources().getIdentifier("drawable/" + country.getCountry(), null, MainApp.getStaticContext().getPackageName()));

            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.getSelectedServer(country);
                }
            });
        }
    }
}