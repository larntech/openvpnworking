package com.webihost.serverproxy.snakevpn.fragmentitems;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anchorfree.partner.api.data.Country;
import com.northghost.hydraclient.R;
import com.pixplicity.easyprefs.library.Prefs;
import com.webihost.serverproxy.snakevpn.MainApp;
import com.webihost.serverproxy.snakevpn.activitys.PremiumActivity;
import com.webihost.serverproxy.snakevpn.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FreeServersFragment extends Fragment {

    @BindView(R.id.freeServersRecyclerview)
    RecyclerView freeServersRecyclerview;

    private PassServerData mCallback;
    private final FreeServersAdapter adapter = new FreeServersAdapter();
    private List<Country> list = new ArrayList<>();

    public static Fragment createInstance() {
        return new FreeServersFragment();
    }

    public void setFreeServersList(List<Country> servers) {
        list = servers;
        adapter.notifyDataSetChanged();
    }

    public void registerCallback(PassServerData passServerData) {
        mCallback = passServerData;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_free_servers, container, false);
        ButterKnife.bind(this, view);
        freeServersRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        freeServersRecyclerview.setAdapter(adapter);
        return view;
    }

    class FreeServersAdapter extends RecyclerView.Adapter<FreeServersViewHolder> {
        @NonNull
        @Override
        public FreeServersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.region_list_item, parent, false);
            return new FreeServersViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull FreeServersViewHolder holder, int position) {
            holder.populateView(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }


    class FreeServersViewHolder extends RecyclerView.ViewHolder {
        TextView mRegionTitle;
        CircleImageView mRegionImage;
        LinearLayout mItemView;
        ImageView premiumImage;
        RadioButton isFreeRadioBtn;

        public FreeServersViewHolder(@NonNull View itemView) {
            super(itemView);
            mRegionTitle = itemView.findViewById(R.id.region_title);
            mRegionImage = itemView.findViewById(R.id.region_image);
            mItemView = itemView.findViewById(R.id.itemView);
            premiumImage = itemView.findViewById(R.id.premiumImage);
            isFreeRadioBtn = itemView.findViewById(R.id.countrySelectedRadioBtn);
        }

        public void populateView(Country country) {
            Locale locale = new Locale("", country.getCountry());
            mRegionTitle.setText(locale.getDisplayCountry());
            mRegionImage.setImageResource(MainApp.getStaticContext().getResources().getIdentifier("drawable/" + country.getCountry(), null, MainApp.getStaticContext().getPackageName()));

            if (Prefs.contains("sname") && Prefs.contains("simage")) {
                String name = Prefs.getString("sname", "");
                Locale localee = new Locale("", name);
                if (localee.getCountry().toLowerCase().equals(locale.getCountry().toLowerCase())) {
                    int textColor = Color.parseColor("#F60303");
                    isFreeRadioBtn.setButtonTintList(ColorStateList.valueOf(textColor));
                    isFreeRadioBtn.setChecked(true);
                } else {
                    isFreeRadioBtn.setChecked(false);
                }
            } else {
                isFreeRadioBtn.setChecked(false);
            }

//            if (locale.getDisplayCountry().equalsIgnoreCase("Unknown server")) {
//            } else if (locale.getDisplayCountry().equalsIgnoreCase("japan") ||
//                    locale.getDisplayCountry().equalsIgnoreCase("Canada") ||
//                    locale.getDisplayCountry().equalsIgnoreCase("united states") ||
//                    locale.getDisplayCountry().equalsIgnoreCase("switzerland") ||
//                    locale.getDisplayCountry().equalsIgnoreCase("australia")) {
                isFreeRadioBtn.setVisibility(View.VISIBLE);
                premiumImage.setVisibility(View.GONE);
//            } else {
//                if (Constants.isPaidVersion) {
//                    isFreeRadioBtn.setVisibility(View.VISIBLE);
//                    premiumImage.setVisibility(View.GONE);
//                } else {
//                    isFreeRadioBtn.setVisibility(View.GONE);
//                    premiumImage.setVisibility(View.VISIBLE);
//                }
//            }

            mItemView.setOnClickListener(v -> {
//                if (locale.getDisplayCountry().equalsIgnoreCase("Unknown server")) {
//                } else if (locale.getDisplayCountry().equalsIgnoreCase("japan") ||
//                        locale.getDisplayCountry().equalsIgnoreCase("Canada") ||
//                        locale.getDisplayCountry().equalsIgnoreCase("united states") ||
//                        locale.getDisplayCountry().equalsIgnoreCase("switzerland") ||
//                        locale.getDisplayCountry().equalsIgnoreCase("australia")) {
                    mCallback.getSelectedServer(country);
//                } else {
//                    if (Constants.isPaidVersion) {
//                        mCallback.getSelectedServer(country);
//                    } else {
//                        startActivity(new Intent(getContext(), PremiumActivity.class));
//                    }
//                }
            });
        }
    }
}