package com.webihost.serverproxy.snakevpn.activitys;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.drawerlayout.widget.DrawerLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.anchorfree.partner.api.response.RemainingTraffic;
import com.anchorfree.sdk.UnifiedSDK;
import com.anchorfree.vpnsdk.callbacks.Callback;
import com.anchorfree.vpnsdk.exceptions.VpnException;
import com.anchorfree.vpnsdk.vpnservice.VPNState;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.material.navigation.NavigationView;
import com.koushikdutta.ion.Ion;
import com.northghost.hydraclient.R;
import com.pixplicity.easyprefs.library.Prefs;
import com.webihost.serverproxy.snakevpn.fragmentitems.ServersFragment;
import com.webihost.serverproxy.snakevpn.utils.BaseActivity;
import com.webihost.serverproxy.snakevpn.utils.Converter;


import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.Gravity.START;

public abstract class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    protected static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.server_ip)
    TextView server_ip;


    @BindView(R.id.connect_btn)
    ImageView vpn_connect_btn;


    @BindView(R.id.uploading_speed)
    TextView uploading_speed_textview;


    @BindView(R.id.downloading_speed)
    TextView downloading_speed_textview;


    @BindView(R.id.vpn_country_image)
    ImageView selectedServerImage;


    @BindView(R.id.vpn_country_name)
    TextView selectedServerName;


    @BindView(R.id.vpn_connection_time)
    ImageView vpn_connection_time;


    @BindView(R.id.vpn_connecting)
    protected LottieAnimationView vpn_connection_state;


    @BindView(R.id.nav_view)
    NavigationView navigationView;


    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;


    private static InterstitialAd mInterstitialAd;


    private final Handler mUIHandler = new Handler(Looper.getMainLooper());


    final Runnable mUIUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            updateUI();
            checkRemainingTraffic();
            mUIHandler.postDelayed(mUIUpdateRunnable, 10000);
        }
    };


    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_main);
        ButterKnife.bind(this);
        MobileAds.initialize(this, initializationStatus -> {
            Map<String, AdapterStatus> statusMap = initializationStatus.getAdapterStatusMap();
            for (String adapterClass : statusMap.keySet()) {
                AdapterStatus status = statusMap.get(adapterClass);
                Log.d("MyApp", String.format(
                        "Adapter name: %s, Description: %s, Latency: %d",
                        adapterClass, status.getDescription(), status.getLatency()));
            }
            loadInters();
            initnativeAd();

        });
        navigationView.findViewById(R.id.servers_menu).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(START)) {
                    drawer.closeDrawer(START);
                } else {
                    drawer.openDrawer(START);
                }
                ServersFragment.newInstance().show(getSupportFragmentManager(), ServersFragment.TAG);
            }
        });
        navigationView.findViewById(R.id.exit_menu).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(START)) {
                    drawer.closeDrawer(START);
                } else {
                    drawer.openDrawer(START);
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this, R.style.AlertDialogTheme);
                builder.setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Exit App")
                        .setMessage("Are you sure you want to exit?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });
    }


    private void loadInters() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, getResources().getString(R.string.home_interstitial_id), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {

                mInterstitialAd = interstitialAd;
                Log.e(TAG, "onAdLoaded");

                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        Log.e("TAG", "The ad was dismissed.");
                        loadInters();

                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                        // Called when fullscreen content failed to show.
                        Log.e("TAG", "The ad failed to show.");
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                Log.e(TAG, loadAdError.getMessage());
                mInterstitialAd = null;
            }
        });
    }


    public void showInterstial() {
        if (mInterstitialAd != null) {
            mInterstitialAd.show(HomeActivity.this);
            AdsCounter = 0;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        isConnected(new Callback<Boolean>() {
            @Override
            public void success(@NonNull Boolean aBoolean) {
                if (aBoolean) {
                    startUIUpdateTask();
                    vpn_connect_btn.setImageResource(R.drawable.ic_on);
                    if (Prefs.contains("sname") && Prefs.contains("simage")) {
                        String name = Prefs.getString("sname", "");
                        Locale locale = new Locale("", name);
                        selectedServerName.setText(locale.getDisplayCountry());
                        selectedServerImage.setImageResource(HomeActivity.this.getResources().getIdentifier("drawable/" + name, null, HomeActivity.this.getPackageName()));
                    }
                }
            }

            @Override
            public void failure(@NonNull VpnException e) {

            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        stopUIUpdateTask();
    }


    protected abstract void isLoggedIn(Callback<Boolean> callback);


    protected abstract void loginToVpn();


    protected abstract void logOutFromVnp();


    @OnClick(R.id.vpn_select_country)
    public void showRegionDialog() {
        ServersFragment.newInstance().show(getSupportFragmentManager(), ServersFragment.TAG);
    }


    @OnClick(R.id.connect_btn)
    public void onConnectBtnClick(View v) {
        vpn_connection_time.setVisibility(View.GONE);
        vpn_connection_state.setVisibility(View.VISIBLE);
        isConnected(new Callback<Boolean>() {
            @Override
            public void success(@NonNull Boolean aBoolean) {
                if (aBoolean) {
                    disconnectFromVnp();
                } else {
                    connectToVpn();
                }
            }

            @Override
            public void failure(@NonNull VpnException e) {

            }
        });
    }


    protected abstract void isConnected(Callback<Boolean> callback);


    protected abstract void connectToVpn();


    protected abstract void disconnectFromVnp();


    protected abstract void chooseServer();


    protected abstract void getCurrentServer(Callback<String> callback);


    protected void startUIUpdateTask() {
        stopUIUpdateTask();
        mUIHandler.post(mUIUpdateRunnable);
    }


    protected void stopUIUpdateTask() {
        mUIHandler.removeCallbacks(mUIUpdateRunnable);
        updateUI();
    }


    protected abstract void checkRemainingTraffic();


    protected void updateUI() {
        UnifiedSDK.getVpnState(new Callback<VPNState>() {
            @Override
            public void success(@NonNull VPNState vpnState) {

                switch (vpnState) {
                    case IDLE: {
                        uploading_speed_textview.setText("0 KB");
                        downloading_speed_textview.setText("0 KB");
                        server_ip.setText(R.string.default_server_ip_text);
                        vpn_connection_state.setVisibility(View.GONE);
                        vpn_connection_time.setVisibility(View.VISIBLE);
                        hideConnectProgress();
                        break;
                    }
                    case CONNECTED: {
                        vpn_connection_state.setVisibility(View.GONE);
                        vpn_connection_time.setVisibility(View.VISIBLE);
                        hideConnectProgress();
                        break;
                    }
                    case CONNECTING_VPN:

                    case CONNECTING_CREDENTIALS:
                    case CONNECTING_PERMISSIONS: {
                        showConnectProgress();
                        break;
                    }
                    case PAUSED: {


                        break;
                    }
                }
            }

            @Override
            public void failure(@NonNull VpnException e) {

            }
        });
        UnifiedSDK.getInstance().getBackend().isLoggedIn(new Callback<Boolean>() {
            @Override
            public void success(@NonNull Boolean isLoggedIn) {
            }

            @Override
            public void failure(@NonNull VpnException e) {

            }
        });

        getCurrentServer(new Callback<String>() {
            @Override
            public void success(@NonNull final String currentServer) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Locale locale = new Locale("", currentServer);
                    }
                });
            }

            @Override
            public void failure(@NonNull VpnException e) {

            }
        });

    }


    protected void updateTrafficStats(long outBytes, long inBytes) {
        String outString = Converter.humanReadableByteCountOld(outBytes, false);
        String inString = Converter.humanReadableByteCountOld(inBytes, false);

        uploading_speed_textview.setText(inString);
        downloading_speed_textview.setText(outString);
    }

    protected void updateRemainingTraffic(RemainingTraffic remainingTrafficResponse) {
        if (remainingTrafficResponse.isUnlimited()) {
        } else {
            String trafficUsed = Converter.megabyteCount(remainingTrafficResponse.getTrafficUsed()) + "Mb";
            String trafficLimit = Converter.megabyteCount(remainingTrafficResponse.getTrafficLimit()) + "Mb";

        }
    }


    protected void ShowIPaddera(String ipaddress) {
        Ion.with(this)
                .load("http://www.ip-api.com/json")
                .asJsonObject()
                .setCallback((e, result) -> {
                    if (result != null) {
                        String ip = result.get("query").getAsString();
                        server_ip.setText(ip);
                    }
                });
    }

    protected void showLoginProgress() {
    }

    protected void hideLoginProgress() {
    }

    protected void showConnectProgress() {

    }

    protected void hideConnectProgress() {

    }

    protected void showMessage(String msg) {
        Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuitem) {

        return true;
    }


    @SuppressLint("MissingPermission")
    private void initnativeAd() {
        AdLoader adLoader = new AdLoader.Builder(this, getResources().getString(R.string.native_ad_id))
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        NativeAdView nativeAdView = (NativeAdView) getLayoutInflater().inflate(R.layout.native_ad_layout, null);
                        mapNativeAdToLayout(nativeAd, nativeAdView);

                        FrameLayout nativeAdLayout = findViewById(R.id.native_ad_frame);
                        nativeAdLayout.removeAllViews();
                        nativeAdLayout.addView(nativeAdView);
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                    }
                })
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void mapNativeAdToLayout(NativeAd nativeAd, NativeAdView nativeAdView) {
        MediaView mediaView = nativeAdView.findViewById(R.id.nativeMedia);
        nativeAdView.setMediaView(mediaView);

        nativeAdView.setHeadlineView(nativeAdView.findViewById(R.id.nativeHeadline));
        nativeAdView.setAdvertiserView(nativeAdView.findViewById(R.id.nativeAdvertiser));
        nativeAdView.setIconView(nativeAdView.findViewById(R.id.nativeIcon));
        nativeAdView.setBodyView(nativeAdView.findViewById(R.id.nativeBody));
        nativeAdView.setPriceView(nativeAdView.findViewById(R.id.nativePrice));
        nativeAdView.setStoreView(nativeAdView.findViewById(R.id.nativeStore));
        nativeAdView.setCallToActionView(nativeAdView.findViewById(R.id.nativeAction));


        ((TextView) nativeAdView.getHeadlineView()).setText(nativeAd.getHeadline());


        if (nativeAd.getAdvertiser() == null) {
            nativeAdView.getAdvertiserView().setVisibility(View.GONE);
        } else {
            ((TextView) nativeAdView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
        }


        if (nativeAd.getIcon() == null) {
            nativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) nativeAdView.getIconView()).setImageDrawable(nativeAd.getIcon().getDrawable());
        }


        if (nativeAd.getBody() == null) {
            nativeAdView.getBodyView().setVisibility(View.GONE);
        } else {
            ((TextView) nativeAdView.getBodyView()).setText(nativeAd.getBody());
        }


        if (nativeAd.getPrice() == null) {
            nativeAdView.getPriceView().setVisibility(View.GONE);
        } else {
            ((TextView) nativeAdView.getPriceView()).setText(nativeAd.getPrice());
        }


        if (nativeAd.getStore() == null) {
            nativeAdView.getStoreView().setVisibility(View.GONE);
        } else {
            ((TextView) nativeAdView.getStoreView()).setText(nativeAd.getStore());
        }


        if (nativeAd.getCallToAction() == null) {
            nativeAdView.getCallToActionView().setVisibility(View.GONE);
        } else {
            ((Button) nativeAdView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }


        nativeAdView.setNativeAd(nativeAd);
    }
}

