package com.webihost.serverproxy.snakevpn.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.anchorfree.partner.api.auth.AuthMethod;
import com.anchorfree.partner.api.response.User;
import com.anchorfree.sdk.UnifiedSDK;
import com.anchorfree.vpnsdk.callbacks.Callback;
import com.anchorfree.vpnsdk.exceptions.VpnException;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.google.android.material.snackbar.Snackbar;
import com.northghost.hydraclient.R;
import com.webihost.serverproxy.snakevpn.utils.Constants;
import com.webihost.serverproxy.snakevpn.utils.UserDefaults;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreenActivity extends AppCompatActivity {


    @BindView(R.id.parent)
    RelativeLayout parent;
    static int SPLASH_SCREEN = 1200;

    BillingClient mBillingClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        UserDefaults.setContext(this);


        new Handler().postDelayed(() -> {
            if (!Utility.isOnline(getApplicationContext())) {

                Snackbar snackbar = Snackbar
                        .make(parent, "Check Your internet connection.", Snackbar.LENGTH_LONG);
                snackbar.show();
            } else {


                checkIfHeIsSubscribed();
            }
        }, SPLASH_SCREEN);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void checkIfHeIsSubscribed() {

        PurchasesUpdatedListener purchasesUpdatedListener = new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(@NonNull @NotNull BillingResult billingResult, @Nullable @org.jetbrains.annotations.Nullable List<Purchase> list) {

            }
        };

        mBillingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases()
                .setListener(purchasesUpdatedListener)
                .build();


        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingServiceDisconnected() {

            }

            @Override
            public void onBillingSetupFinished(@NonNull @NotNull BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    UserDefaults.standard().putBoolean(Constants.product_monthly, false);
                    UserDefaults.standard().putBoolean(Constants.product_yearly, false);
                    List<Purchase> subsResult = mBillingClient.queryPurchases(BillingClient.SkuType.SUBS).getPurchasesList();
                    for (Purchase p : subsResult) {
                        if (p.getSkus().equals(Constants.product_monthly)) {
                            UserDefaults.standard().putBoolean(Constants.product_monthly, true);
                        }
                        if (p.getSkus().equals(Constants.product_yearly)) {
                            UserDefaults.standard().putBoolean(Constants.product_yearly, true);
                        }
                    }
                }
                loginUser();
            }
        });

    }


    private void loginUser() {
        boolean monthly = UserDefaults.standard().getBoolean(Constants.product_monthly, false);
        boolean yearly = UserDefaults.standard().getBoolean(Constants.product_yearly, false);

        Constants.isPaidVersion = (monthly) || (yearly);

        AuthMethod authMethod = AuthMethod.anonymous();
        UnifiedSDK.getInstance().getBackend().login(authMethod, new Callback<User>() {
            @Override
            public void success(@NonNull User user) {
                Intent intent_main = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent_main);
                finish();
            }

            @Override
            public void failure(@NonNull VpnException e) {
                Snackbar snackbar = Snackbar.make(parent, "Authentication Error, Please try again.", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("Try again", v -> loginUser());
                snackbar.show();
            }
        });
    }
}
