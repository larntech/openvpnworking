package com.webihost.serverproxy.snakevpn.activitys;

import static com.webihost.serverproxy.snakevpn.utils.Constants.product_monthly;
import static com.webihost.serverproxy.snakevpn.utils.Constants.product_yearly;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.northghost.hydraclient.R;
import com.webihost.serverproxy.snakevpn.utils.Constants;
import com.webihost.serverproxy.snakevpn.utils.UserDefaults;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.ButterKnife;

public class PremiumActivity extends AppCompatActivity {
    CardView monthlyBtnLay, yearlyBtnLay;
    RelativeLayout buyPremiumBtn;
    RadioButton monthlyBtn, yearlyBtn;
    BillingClient mBillingClient;
    SkuDetails skuDetails;
    String sku;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);
        ButterKnife.bind(this);


        monthlyBtn = findViewById(R.id.monthlyRadioBtn);
        yearlyBtn = findViewById(R.id.yearlyRadioBtn);
        monthlyBtnLay = findViewById(R.id.monthlyBtnLay);
        yearlyBtnLay = findViewById(R.id.yearlyBtnLay);
        buyPremiumBtn = findViewById(R.id.buyPremiumBtn);

        UserDefaults.setContext(this);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                Map<String, AdapterStatus> statusMap = initializationStatus.getAdapterStatusMap();
                for (String adapterClass : statusMap.keySet()) {
                    AdapterStatus status = statusMap.get(adapterClass);
                    Log.d("MyApp", String.format(
                            "Adapter name: %s, Description: %s, Latency: %d",
                            adapterClass, status.getDescription(), status.getLatency()));
                }
                showBannerAd();

            }


        });

        yearlyBtnLay.setBackgroundTintList(null);
        monthlyBtnLay.setOnClickListener(v -> {
            yearlyBtn.setChecked(false);
            monthlyBtn.setChecked(true);
            yearlyBtnLay.setBackgroundTintList(null);
            monthlyBtnLay.getBackground().setTint(getResources().getColor(R.color.selected_payment_color));
        });

        yearlyBtnLay.setOnClickListener(v -> {
            yearlyBtn.setChecked(true);
            monthlyBtn.setChecked(false);
            monthlyBtnLay.setBackgroundTintList(null);
            yearlyBtnLay.getBackground().setTint(getResources().getColor(R.color.selected_payment_color));
        });

        buyPremiumBtn.setOnClickListener(v -> {
            if (Constants.isPaidVersion) {
                Toast.makeText(this, "Subscription Already Subscribed", Toast.LENGTH_SHORT).show();
                return;
            }
            if (monthlyBtn.isChecked()) {
                List<String> skuList = new ArrayList<>();
                skuList.add(product_monthly);

                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
                mBillingClient.querySkuDetailsAsync(params.build(),
                        (billingResult, skuDetailsList) -> {

                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                                for (Object skuDetailsObject : skuDetailsList) {
                                    skuDetails = (SkuDetails) skuDetailsObject;
                                    sku = skuDetails.getSku();

                                    if (product_monthly.equals(sku)) {
                                        BillingFlowParams flowParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetails).build();
                                        mBillingClient.launchBillingFlow(Objects.requireNonNull(Objects.requireNonNull(PremiumActivity.this)), flowParams);

                                    } else {
                                        Log.d("TAG", "Sku is null");
                                    }

                                }
                            }
                        });

            } else if (yearlyBtn.isChecked()) {
                List<String> skuList = new ArrayList<>();
                skuList.add(product_yearly);

                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
                mBillingClient.querySkuDetailsAsync(params.build(),
                        (billingResult, skuDetailsList) -> {

                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                                for (Object skuDetailsObject : skuDetailsList) {
                                    skuDetails = (SkuDetails) skuDetailsObject;
                                    sku = skuDetails.getSku();

                                    if (product_yearly.equals(sku)) {
                                        BillingFlowParams flowParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetails).build();
                                        mBillingClient.launchBillingFlow(Objects.requireNonNull(Objects.requireNonNull(PremiumActivity.this)), flowParams);

                                    } else {
                                        Log.d("TAG", "Sku is null");
                                    }

                                }
                            }
                        });
            }

        });

        PurchasesUpdatedListener purchasesUpdatedListener = (billingResult, purchases) -> {
            if (purchases != null) {
                for (final Purchase p : purchases) {
                    if (p.getSkus().equals(product_monthly)) {
                        UserDefaults.standard().putBoolean(product_monthly, true);
                    }
                    if (p.getSkus().equals(Constants.product_yearly)) {
                        UserDefaults.standard().putBoolean(Constants.product_yearly, true);
                    }
                }

                boolean monthly = UserDefaults.standard().getBoolean(product_monthly, false);
                boolean yearly = UserDefaults.standard().getBoolean(Constants.product_yearly, false);

                Constants.isPaidVersion = (monthly) || (yearly);

                if (Constants.isPaidVersion) {
                    Toast.makeText(PremiumActivity.this, "Purchase Successful", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(PremiumActivity.this, "Something went Wrong while purchasing...", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(PremiumActivity.this, "Failed to purchase", Toast.LENGTH_SHORT).show();
            }
        };

        mBillingClient = BillingClient.newBuilder(this)
                .setListener(purchasesUpdatedListener)
                .enablePendingPurchases()
                .build();

        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingServiceDisconnected() {

            }

            @Override
            public void onBillingSetupFinished(@NonNull @NotNull BillingResult billingResult) {

            }
        });


    }

    @SuppressLint("MissingPermission")
    private void showBannerAd() {
        MobileAds.initialize(this, initializationStatus -> {
        });

        AdView mAdView = findViewById(R.id.adView_prem);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
}