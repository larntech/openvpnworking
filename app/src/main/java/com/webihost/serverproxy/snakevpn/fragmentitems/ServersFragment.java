package com.webihost.serverproxy.snakevpn.fragmentitems;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.airbnb.lottie.LottieAnimationView;
import com.anchorfree.partner.api.data.Country;
import com.anchorfree.partner.api.response.AvailableCountries;
import com.anchorfree.sdk.UnifiedSDK;
import com.anchorfree.vpnsdk.callbacks.Callback;
import com.anchorfree.vpnsdk.exceptions.VpnException;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.tabs.TabLayout;
import com.northghost.hydraclient.R;
import com.pixplicity.easyprefs.library.Prefs;
import com.webihost.serverproxy.snakevpn.adapters.TabsAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServersFragment extends DialogFragment implements PassServerData {

    public static final String TAG = ServersFragment.class.getSimpleName();

    @BindView(R.id.servers_tablayout)
    TabLayout serversTablayout;
    @BindView(R.id.servers_viewPager)
    ViewPager serversViewPager;
    @BindView(R.id.activity_name)
    TextView activityName;
    @BindView(R.id.regions_progress)
    ProgressBar regionsProgressBar;
    private TabsAdapter adapter;
    private final List<Country> freeList = new ArrayList<>();
    private final List<Country> paidList = new ArrayList<>();
    private RegionChooserInterface mCallback;

    public static ServersFragment newInstance() {
        ServersFragment frag = new ServersFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main_servers, container, false);
        ButterKnife.bind(this, view);
        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                Map<String, AdapterStatus> statusMap = initializationStatus.getAdapterStatusMap();
                for (String adapterClass : statusMap.keySet()) {
                    AdapterStatus status = statusMap.get(adapterClass);
                    Log.d("MyApp", String.format(
                            "Adapter name: %s, Description: %s, Latency: %d",
                            adapterClass, status.getDescription(), status.getLatency()));
                }

                showBannerAd();

            }

            @SuppressLint("MissingPermission")
            private void showBannerAd() {
                MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
                    @Override
                    public void onInitializationComplete(InitializationStatus initializationStatus) {
                    }
                });
                AdView mAdView = view.findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);

            }


        });


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activityName.setText("SERVERS");
        getServers();
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        if (ctx instanceof RegionChooserInterface) {
            mCallback = (RegionChooserInterface) ctx;
        }
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        if (fragment instanceof FreeServersFragment) {
            FreeServersFragment freeServersFragment = (FreeServersFragment) fragment;
            freeServersFragment.setFreeServersList(freeList);
            freeServersFragment.registerCallback(this);
        } else if (fragment instanceof TopServersFragment) {
            TopServersFragment vipServersFragment = (TopServersFragment) fragment;
            vipServersFragment.setVIPServersList(paidList);
            vipServersFragment.registerCallback(this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    private void getServers() {
        showProgress();

        UnifiedSDK.getInstance().getBackend().countries(new Callback<AvailableCountries>() {
            @Override
            public void success(@NonNull final AvailableCountries countries) {
                for (Country country : countries.getCountries()) {
                    Locale locale = new Locale("", country.getCountry());
                    if (locale.getDisplayCountry().equalsIgnoreCase("Unknown server")) {
                    }
                    else if (locale.getDisplayCountry().equalsIgnoreCase("japan") ||
                            locale.getDisplayCountry().equalsIgnoreCase("Canada") ||
                            locale.getDisplayCountry().equalsIgnoreCase("united states") ||
                            locale.getDisplayCountry().equalsIgnoreCase("switzerland") ||
                            locale.getDisplayCountry().equalsIgnoreCase("australia")) { freeList.add(country); }
                    else {
                        paidList.add(country);
                    }


                }
                Collections.reverse(freeList);
                freeList.addAll(paidList);
                adapter = new TabsAdapter(getChildFragmentManager());
                adapter.addFragment(FreeServersFragment.createInstance(), "Free Server");
                serversViewPager.setAdapter(adapter);
                serversTablayout.setupWithViewPager(serversViewPager);
                hideProress();

            }

            @Override
            public void failure(VpnException e) {
                dismiss();
                hideProress();

            }
        });
    }
    private void showProgress() {
        serversViewPager.setVisibility(View.INVISIBLE);

        regionsProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProress() {
        regionsProgressBar.setVisibility(View.GONE);
        serversViewPager.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.finish_activity)
    public void onViewClicked() {
        dismiss();
    }

    @Override
    public void getSelectedServer(Country country) {
        Prefs.putString("sname", country.getCountry());
        Prefs.putString("simage", country.getCountry());

        mCallback.onRegionSelected(country);
        dismiss();
    }

    public interface RegionChooserInterface {
        void onRegionSelected(Country item);
    }
}
